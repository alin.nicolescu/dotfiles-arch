" Basic .vimrc (for Linux)
""""""""""""""""""""""""""""""

" Set
set nocompatible
filetype plugin on
filetype indent on
syntax on
set encoding=utf-8
set fileencoding=utf-8
set path+=**
set wildmenu
set wildmode=longest,list,full
set cm=blowfish2
set showcmd
set showmode
set nohlsearch
set incsearch
set ignorecase
set smartcase
set nostartofline
set ruler
set number
set laststatus=2
"set confirm
set visualbell
set shiftwidth=4
set softtabstop=4
set expandtab
"set cursorline
set scrolloff=5
set report=1
set backspace=indent,eol,start
set colorcolumn=81
set nowrap
set showmatch
set splitbelow splitright
set listchars=tab:▸\ ,eol:¬
"set listchars=tab:»·,eol:¬,nbsp:·,trail:·,extends:>,precedes:<
set t_Co=256
"colorscheme lunaperche
colorscheme habamax

" Netrw
let g:netrw_winsize=25
let g:netrw_banner=0

" Autocommands
"" No automatic comment on new line
autocmd FileType * setlocal formatoptions-=c formatoptions-=r formatoptions-=o
"" Syntax highlight for alias files
autocmd BufNewFile,BufRead .*aliases* set ft=sh
"" Don't expand tab in Makefiles
autocmd FileType make setlocal noexpandtab
"" Compiling
autocmd filetype c nnoremap <Leader>. <Esc>:w<CR>:!clear && gcc % -o %:r && ./%:r<CR>
autocmd filetype cpp nnoremap <Leader>. <Esc>:w<CR>:!clear && g++ % -o %:r && ./%:r<CR>
autocmd filetype tex nnoremap <Leader>. <Esc>:w<CR>:!clear && pdflatex %<CR>

" Maps
nnoremap <C-l> :nohl<CR><C-L>
nnoremap <C-n> :Lexplore<CR>
nnoremap j gj
nnoremap k gk
inoremap jj <ESC>
"" Tabs
nnoremap tn :tabnew<CR>
nnoremap tj :tabnext<CR>
nnoremap tk :tabprevious<CR>
"" Buffers
nnoremap bj :bnext<CR>
nnoremap bk :bprevious<CR>
"" Insert an actual TAB with Shift+TAB
inoremap <S-Tab> <C-V><Tab>
"" Resize splits
noremap <silent> <C-Left> :vertical resize +3<CR>
noremap <silent> <C-Right> :vertical resize -3<CR>
noremap <silent> <C-Up> :resize -3<CR>
noremap <silent> <C-Down> :resize +3<CR>
"" Moving lines
nnoremap <silent> <A-Down> :m .+1<CR>==
nnoremap <silent> <A-Up> :m .-2<CR>==
inoremap <silent> <A-Down> <Esc>:m .+1<CR>==gi
inoremap <silent> <A-Up> <Esc>:m .-2<CR>==gi
vnoremap <silent> <A-Down> :m '>+1<CR>gv=gv
vnoremap <silent> <A-Up> :m '<-2<CR>gv=gv
"" Leader key
let mapleader=" "
nnoremap <Leader>r :set relativenumber!<CR>
nnoremap <Leader>s :setlocal spell! spelllang=en_us,ro<CR>
map <Leader>l :set list!<CR>
