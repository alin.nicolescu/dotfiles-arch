alias tmux='tmux -u -2'
alias ls='ls --color=auto'
alias la='ls -la --color=always --group-directories-first'
alias ll='ls -l --color=always --group-directories-first'
alias mirrors='sudo reflector --verbose --latest 100 --protocol https --sort rate --save /etc/pacman.d/mirrorlist'
alias git-log="git log --graph --pretty=format:'%Cred%h%Creset -%C(yellow)%d%Creset %s %Cgreen(%cr) %C(bold blue)<%an>%Creset' --abbrev-commit"
alias git-simple-log="git log --pretty=oneline --abbrev-commit"
alias dmenu='dmenu -fn "monospace:size=16"'
alias xopen='xdg-open'
alias sensors='watch sensors'
alias diff='diff --color=auto'
alias grep='grep --color=auto'
alias ip='ip -color=auto'

