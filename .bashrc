#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

# Options
shopt -s checkwinsize
shopt -s no_empty_cmd_completion
shopt -s histappend
shopt -s expand_aliases
export HISTCONTROL=ignoredups:erasedups

# Completion
if [ -f /usr/share/bash-completion/bash_completion ]; then
    . /usr/share/bash-completion/bash_completion
fi

# Aliases
if [ -f "$HOME/.bash_aliases" ]; then
    . "$HOME/.bash_aliases"
fi

# PATH
if [ -d "$HOME/.local/bin" ] ; then
    export PATH="$HOME/.local/bin:$PATH"
fi

## LaTeX
export MANPATH="$HOME/opt/texlive/2024/texmf-dist/doc/man:$MANPATH"
export INFOPATH="$HOME/opt/texlive/2024/texmf-dist/doc/info:$INFOPATH"
export PATH="$HOME/opt/texlive/2024/bin/x86_64-linux:$PATH"

# GPG
export GPG_TTY=$(tty)

## Variables
export EDITOR=vim
export PAGER=less
export VISUAL=vim
export BROWSER=chromium
export NNN_OPTS="deRo"

## Man pager
export MANPAGER="less -R --use-color -Dd+r -Du+b"
export MANROFFOPT="-P -c"

# Keyboard
setxkbmap ro

# Colors
RESET="\e[0m"
BOLD="\e[1m"
# DIM="\e[2m"
# UNDERLINE="\e[4m"
# BLINK="\e[5m"
# REVERSE="\e[7m"
# HIDDEN="\e[8m"
# DEFAULT="\e[39m"
# BLACK="\e[30m"
RED="\e[31m"
GREEN="\e[32m"
# YELLOW="\e[33m"
BLUE="\e[34m"
# MAGENTA="\e[35m"
# CYAN="\e[36m"
# LIGHT_GRAY="\e[37m"
# DARK_GRAY="\e[90m"
# LIGHT_RED="\e[91m"
# LIGHT_GREEN="\e[92m"
# LIGHT_YELLOW="\e[93m"
# LIGHT_BLUE="\e[94m"
# LIGHT_MAGENTA="\e[95m"
# LIGHT_CYAN="\e[96m"
# WHITE="\e[97m"
# RED_BACKG="\e[41m"
# GREEN_BACKG="\e[42m"
# YELLOW_BACKG="\e[43m"
# BLUE_BACKG="\e[44m"
# MAGENTA_BACKG="\e[45m"
# CYAN_BACKG="\e[46m"
# DARK_GRAY_BACKG="\e[100m"
# LIGHT_BLUE_BACKG="\e[104m"

# Prompt
#PS1='[\u@\h \W]\$ '
#PS1="\[$BOLD\]\[$GREEN\]\u @ \h\[$RESET\] (\[$BOLD\]\[$BLUE\]\W\[$RESET\]) \$ "
# export PS1="\[$GREEN\]\h\[$RESET\] [\[$BOLD\]\[$BLUE\]\w\[$RESET\]] \$ "
export PS1="$GREEN\H$RESET [$BLUE\w$RESET] $RED\$$RESET "

